class CreateTradeDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :trade_details do |t|
      t.integer :user_id
      t.integer :trade_id
      t.decimal :ether
      t.decimal :token_amount
      t.text :eth_send_detail
      t.text :token_send_detail
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
