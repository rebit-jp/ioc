class CreateTrades < ActiveRecord::Migration[5.0]
  def change
    create_table :trades do |t|
      t.integer :user_id
      t.integer :token_id

      t.timestamps
    end
  end
end
