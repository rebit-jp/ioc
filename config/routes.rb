Rails.application.routes.draw do
  resources :tokens, only: %i[index show new create] do
    patch :trade, on: :member
  end
  resources :trades, only: %i[index show]
  devise_for :users
  root 'top#index'
end
