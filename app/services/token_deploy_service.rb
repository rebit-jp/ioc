require 'ethereum'

class TokenDeployService
  def initialize(from_user, token)
    @from_user = from_user
    @client = Ethereum::IpcClient.new(Token::ETHEREUM_PATH)
    @from_address = from_user.address
    @name = token.name
    @symbol = token.symbol
    @total_tokens = token.totalTokens
  end

  def perform
    password = @from_user.ether_password.presence || ''
    @client.personal_unlock_account(@from_address, password)
    address = deploy_token(@from_address, @name, @symbol, @total_tokens)
    address
  end

  private

  def deploy_token(from_address, name, symbol, total_tokens)
    contract = Ethereum::Contract.create(file: Token::ETHEREUM_TOKEN_PATH, client: @client)
    address = contract.deploy_and_wait(from_address, name, symbol, total_tokens.to_i)
    address
  end
end
