require 'ethereum'

class TokenSendService
  def initialize(from_user, to_user, token, amount)
    @from_user = from_user
    @to_user = to_user
    @client = Ethereum::IpcClient.new(Token::ETHEREUM_PATH)
    @token = token
    @amount = amount.to_i
  end

  def perform
    @client.personal_unlock_account(@from_user.address, @from_user.ether_password)
    address = send_token(@from_user.address, @to_user.address, @token.token_address, @amount)
    @client.eth_get_transaction_by_hash(address)['result']
  end

  private

  def send_token(from_address, to_address, token_address, amount)
    contract = Ethereum::Contract.create(file: Token::ETHEREUM_TOKEN_PATH, client: @client,
                                         address: token_address, abi: Token::ERC20TOKEN_ABI)
    object = contract.transact_and_wait.transfer_from(from_address, to_address, amount)
    object.id
  end
end
