class PossessionUpdateService
  def initialize(buy_user, sale_user, token, token_amount)
    @buy_user = buy_user
    @sale_user = sale_user
    @token = token
    @token_amount = token_amount.to_i
  end

  def perform
    create_or_update_buyer_possession
    delete_or_update_saler_possession
  end

  private

  def create_or_update_buyer_possession
    @possession = Possession.find_or_initialize_by(token_id: @token.id, user_id: @buy_user.id)
    @possession.balance = (@possession.balance.presence || 0) + @token_amount
    @possession.save
  end

  def delete_or_update_saler_possession
    @possession = Possession.find_by(token_id: @token.id, user_id: @sale_user.id)
    return if @possession.blank?
    balance = @possession.balance - @token_amount
    @possession.update(balance: balance)
    @possession.destroy if @possession.balance.zero?
  end
end
