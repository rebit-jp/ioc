class TradeService
  def initialize(buy_user, token, trade)
    @buy_user = buy_user
    @token = token
    @trade = trade
    @users = User.where(id: @trade.trade_details.map(&:user_id)).group_by(&:id)
  end

  def perform
    Trade.transaction do
      execute_trade
      @trade.save
    end
    true
  rescue StandardError => e
    Rails.logger.info(e.message)
    false
  end

  private

  def execute_trade
    @trade.trade_details.map do |td|
      td.delete if td.token_amount.blank?
      sale_user = @users[td.user_id].first
      eth_value = @token.price.to_i * td.token_amount.to_i
      eth_send_detail = EthSendService.new(@buy_user, sale_user, eth_value).perform
      token_send_detail = TokenSendService.new(sale_user, @buy_user, @token, td.token_amount).perform
      PossessionUpdateService.new(@buy_user, sale_user, @token, td.token_amount).perform
      td.ether = eth_value
      td.eth_send_detail = eth_send_detail.to_json
      td.token_send_detail = token_send_detail.to_json
    end
  end
end
