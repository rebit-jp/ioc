# frozen_string_literal: true

# == Schema Information
#
# Table name: tokens
#
#  id            :integer          not null, primary key
#  name          :string
#  symbol        :string
#  totalTokens   :decimal(, )
#  token_address :string
#  owner_id      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  price         :integer
#

require 'ethereum'

class Token < ApplicationRecord
  has_many :trades, dependent: :destroy
  has_many :possessions, dependent: :destroy

  validates :name, presence: true, uniqueness: true
  validates :symbol, presence: true, uniqueness: true
  validates :totalTokens, numericality: { only_integer: true, greater_than: 0 }

  ETHEREUM_PATH = '/Users/rebit_n005/testnet/geth.ipc'
  ETHEREUM_TOKEN_PATH = "#{Dir.pwd}/contracts/ERC20Token.sol"
  ERC20TOKEN_ABI = <<-ABI
  [
    {"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},
    {"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_amount","type":"uint256"}],
    "name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},
    {"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"totalSupply","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},
    {"constant":false,"inputs":[{"name":"_owner","type":"address"},{"name":"_to","type":"address"},{"name":"_amount","type":"uint256"}],"name":"transferInternal",
    "outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},
    {"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_amount","type":"uint256"}],"name":"transferFrom",
    "outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},
    {"constant":true,"inputs":[],"name":"floats","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},
    {"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},
    {"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},
    {"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},
    {"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_amount","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},
    {"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},
    {"inputs":[{"name":"_owner","type":"address"},{"name":"_name","type":"string"},{"name":"_symbol","type":"string"},{"name":"_totalTokens","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},
    {"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},
    {"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}
  ]
  ABI

  def total_supply
    @client = Ethereum::IpcClient.new(ETHEREUM_PATH)
    user = User.first
    @client.personal_unlock_account(user.address, user.ether_password)
    contract = Ethereum::Contract.create(file: ETHEREUM_TOKEN_PATH, client: @client,
                                         address: token_address, abi: ERC20TOKEN_ABI)
    contract.call.total_supply
  end

  def balance_of(user)
    @client = Ethereum::IpcClient.new(ETHEREUM_PATH)
    # @client.personal_unlock_account(user.address, user.ether_password)
    contract = Ethereum::Contract.create(file: ETHEREUM_TOKEN_PATH, client: @client,
                                         address: token_address, abi: ERC20TOKEN_ABI)
    contract.call.balance_of(user.address)
  end

  def balance(user)
    possession = possessions.group_by(&:user_id)[user.id]
    possession.present? ? possession.first.balance : 0
  end
end
