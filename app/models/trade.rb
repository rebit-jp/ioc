# == Schema Information
#
# Table name: trades
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  token_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Trade < ApplicationRecord
  belongs_to :user
  belongs_to :token
  has_many :trade_details, dependent: :destroy
  validate :token_amount_check
  validate :ether_check

  accepts_nested_attributes_for :trade_details

  scope :list, lambda {
    includes(:user, :token, trade_details: :user).order(created_at: :desc)
  }

  def to_user_emails
    trade_details.map { |td| td.user.email }
  end

  private

  def token_amount_check
    trade_details.each do |td|
      errors.add(:token_amount, ': トークン所持量をオーバーしています') if td.token_amount.to_i > token.balance(user)
    end
  end

  def ether_check
    trade_details.each do |td|
      eth_value = token.price.to_i * td.token_amount.to_i
      errors.add(:ether, ': Etherが不足しています') if eth_value > user.eth_balance
    end
  end
end
