class TokensController < ApplicationController
  before_action :set_token, only: %i[show trade]
  before_action :check_ico_client, only: %i[new create]

  def index
    @tokens = Token.all.includes(:possessions)
  end

  def show
    @trade = @token.trades.build
    @trade.trade_details.build
    @users = User.where.not(id: current_user.id)
  end

  def new
    @token = Token.new
  end

  def create
    @token = Token.new(tokens_params)
    @token.owner_id = current_user.id
    if @token.valid?
      token_address = TokenDeployService.new(current_user, @token).perform
      @token.token_address = token_address
      @possession = @token.possessions.build(user: current_user, balance: @token.totalTokens)
      if token_address.present? && @token.save && @possession.save
        redirect_to tokens_path, notice: 'Success Create Token!'
      else
        render 'new'
      end
    else
      render 'new'
    end
  end

  def trade
    @trade = @token.trades.build(trade_params)
    @trade.user = current_user
    @trade.trade_details.each { |td| td.delete if td.token_amount.blank? }
    if @trade.valid? && @trade.trade_details.present?
      ts_success_flg = TradeService.new(current_user, @trade.token, @trade).perform
      if ts_success_flg
        redirect_to trades_path, notice: 'Trade is success!'
      else
        redirect_to token_path(@token)
      end
    else
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def set_token
    @token = Token.find(params[:id])
  end

  def tokens_params
    params.require(:token).permit(:name, :symbol, :totalTokens, :price)
  end

  def trade_params
    params.require(:trade).permit(trade_details_attributes: %i[user_id token_amount])
  end
end
