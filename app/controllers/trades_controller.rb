class TradesController < ApplicationController
  before_action :set_trade, only: %i[show]

  def index
    @trades = Trade.list.page(params[:page]).per(50)
  end

  def show
    @trade_details = @trade.trade_details.includes(:user)
  end

  private

  def set_trade
    @trade = Trade.find(params[:id])
  end
end
