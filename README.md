# Ethereum Token Trader
このアプリケーションはEthereumのETHとトークンを交換するアプリです。
本アプリは以下のオープンソースのアプリを参考に作成しました。
https://github.com/shiki-tak/token-trader-app

トークンはEthereumが提唱しているトークンの標準仕様「ERC20」に準拠しています。

【要件】
- トークンの新規作成(ICOクライアントのみ)
- トークンの購入

## Requirements
- Ruby 2.4.2
- Rails 5.0.6
- sqlite3
- go-ethereum 1.8.4
- ethereum.rb 2.1.7
 (https://github.com/EthWorks/ethereum.rb.git)

## Set up
※ OSはMacを想定しています。  

### 1. Gethのインストール
  ※ gethインストールは https://www.newscrypto.jp/articles/584 の方法を参考にしました。

  1. go-ethereumをgitリポジトリからクローンする  
  $ git clone https://github.com/ethereum/go-ethereum  
  $ brew install go  
  $ cd go-ethereum  
  $ make geth  
  $ cd build/bin  
  $ mv geth /usr/local/bin  

### 2. テストネットワークでGethを起動する
  1. データディレクトリを準備する  
  $ mkdir ~/data_testnet  
  $ cd data_testnet/  
  $ pwd  
  \# /home/eth/data_testnet  

  2. Genesisファイルを作成する  
  $ mkdir ~/testnet  
  $ cd ~/testnet  
  $ vi genesis.json  
  {  
    "config": {  
    "chainId": 23,  
    "homesteadBlock": 0,  
    "eip155Block": 0,  
    "eip158Block": 0  
    },  
    "nonce": "0x0000000000000042",  
    "timestamp": "0x0",  
    "parentHash":  
    "0x0000000000000000000000000000000000000000000000000000000000000000",  
    "extraData": "0x00",  
    "gasLimit": "0x8000000",  
    "difficulty": "0x400",  
    "mixhash": "0x0000000000000000000000000000000000000000000000000000000000000000",  
    "coinbase": "0x3333333333333333333333333333333333333333",  
    "alloc": {  
    }  
  }  

  3. Gethを初期化する  
  $ geth --datadir ~/testnet init genesis.json  
  
  4. Gethを起動する  
  $ geth --networkid 23 --nodiscover --maxpeers 0 --datadir ~/testnet  
  Fatal: Error starting protocol stack: database already contains an incompatible genesis block (have 33bbd028dcb05e17, new e5be92145a301820)
  のようなエラーが出る場合はgeth配下のchaindataを削除する  
  $ rm -rf geth/chaindata  
  
  5. 以下のコマンドでGethのコンソールに接続して正常に起動しているか確認する  
  $ cd ~/testnet  
  $ geth attach geth.ipc  


### 3. Smart contract開発用のコンパイラをインストール
  1. 以下のコマンドを実行してSolidityのコンパイラをインストールする。  
  $ brew update  
  $ brew upgrade  
  $ brew tap ethereum/ethereum  
  $ brew install solidity  
  $ brew linkapps solidity  
  
  2. インストールされたことを確認する。  
  $ solc --version  
  solc, the solidity compiler commandline interface  
  Version: 0.4.15+commit.8b45bddb.Darwin.appleclang  
  
  3. solcのPATHを確認する。  
  $ which solc  
  /usr/local/bin/solc  
  
  4. admin.setSolcコマンドでGethにsolcのパスをセットする。  
  \> admin.setSolc("/usr/bin/solc")  
  正しく、セットされたことを確認する。  
  \> eth.getCompilers()  
  \# ["Solidity"]  
  

### 4. アプリケーションの設定
  以下のコマンドを実行し、アプリケーションを起動する。  
  $ git clone https://rebit_sugiyama:testtest@bitbucket.org/rebit-jp/ioc.git  
  $ bundle install  
  $ rake db:create  
  $ rake db:migrate  
  $ rails s  
  ※ token.rbの"ETHEREUM_PATH"を環境に合わせて変更する。  

※注意点  
トークン作成中にauthentication needed: password or unlockのエラーが出る場合があります。  
コンソール上で  
personal.unlockAccount(eth.accounts[0])  
などのコマンドを実行し、ロックを解除するようにしてください。
